<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->load->view('login');
	}

	public function login()
	{
        $email = $this->input->post('email');
        $password = $this->input->post('password');
		
        $user = $this->db->get_where('register', array('email' => $email, 'password' => $password, 'is_active' => 1	, 'is_varified' => 1))->first_row();

		/* if valid user */	
        if(!empty($user)) {
			$this->session->set_userdata('user_id', $user->id);
			if($user->is_admin == 1) {
				redirect('dashboard/admindashboard');
			} else {
				redirect('dashboard/userdashboard');
			}
        } else {
            redirect('login');
        }

		/* if valid admin */
		if($admin > 0) {
        } else {
            redirect('login');
        }
	}

    public function emailVarify($id)
	{
        $data = array(
            'is_varified' => $id,
        );
        
		$insert = $this->db->insert('register', $data);
	}
}
