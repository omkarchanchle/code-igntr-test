<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegistrationController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->load->view('registration');
	}

	public function store()
	{
		$data = array(
            'first_name' => $this->input->post('firstname'),
            'last_name' => $this->input->post('lastname'),
			'email' => $this->input->post('email'),
            'password' => $this->input->post('password'),
            'is_active' => 1,
            'is_varified' => 1,
        );
        
		$insert = $this->db->insert('register', $data);

		if($insert) {
			$this->sendEmail($data['email'], $this->db->insert_id());
			redirect('login');
		} else {
			redirect('registration');
		}
	}

	public function sendEmail($email, $id)
	{

		// $from_email = "omkar.mtzinfotech@gmail.com";
        // $to_email = $email;
        // //Load email library
        // $this->load->library('email');
        // $this->email->from($from_email, 'Identification');
        // $this->email->to($to_email);
        // $this->email->subject('Send Email Codeigniter');
        // $this->email->message("Dear User,\nPlease click on below URL or paste into your browser to verify your Email Address\n\n ".base_url("LoginController/emailVarify/$id")."\n"."\n\nThanks\nAdmin Team");
        // //Send mail
        // if($this->email->send())
        //     $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
        // else
        //     $this->session->set_flashdata("email_sent","You have encountered an error");
        // $this->load->view('registration');

		// $config = Array(
		// 	'protocol' => 'smtp',
		// 	// 'smtp_host' => 'smtp.gmail.com',
		// 	'smtp_host' => 'tls://smtp.gmail.com',
		// 	'smtp_port' => 587,
		// 	// 'smtp_port' => 465,
		// 	'smtp_user' => 'omkar.mtzinfotech@gmail.com', // change it to yours
		// 	'smtp_pass' => '*()</>phpmysqljs(omkar)', // change it to yours
		// 	'charset' => 'iso-8859-1',
		// 	'mailtype' => 'html',
		// 	// 'smtp_secure' => 'tls',
		// 	'wordwrap' => TRUE
		//  );
		  
		// $this->email->initialize($config);
		// //  $this->load->library('email', $config);
		//  $this->email->set_newline("\r\n");
		//  $this->email->from('omkar.mtzinfotech@gmail.com', "Admin Team");
		//  $this->email->to($email);  
		//  $this->email->subject("Email Verification");
		//  $this->email->message("Dear User,\nPlease click on below URL or paste into your browser to verify your Email Address\n\n ".base_url("LoginController/emailVarify/$id")."\n"."\n\nThanks\nAdmin Team");
		//  $x = $this->email->send();
		//  echo'<pre>';print_r($x);exit;
	}
}
