<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User dashboard</title>
    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>
</head>
<body>
    <table>
        <thead>
            <th>Id</th>
            <th>Name</th>
            <th>Image</th>
            <th>Quantity</th>
            <th>Add to Cart</th>
        </thead>
        <tbody>
        <?php 
            $data = $this->db->get('product')->result();
            foreach($data as $row) { 
        ?>
            <tr>
                <td><?php echo $row->id; ?></td>
                <td><?php echo $row->name; ?></td>
                <td><img src="<?php echo base_url('uploads/'.$row->image); ?>" alt="<?php echo $row->name; ?>" width="100" height="100"></td>
                <td>
                    <button class="desc"> - </button>
                        <input type="hidden" id="product_id" value="$row->id">
                        <input type="number" id="quantity" value="1">
                    <button class="asc"> + </button>
                </td>
                <td>                    
                    <button class="add_cart"> ADD </button>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</body>

<script src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>

<script>
    $(document).ready(function() {
        $('.desc').click(function() {
            var quantity = $(this).parent().find('#quantity').val();
            quantity--;
            $(this).parent().find('#quantity').val(quantity);
        });

        $('.asc').click(function() {
            var quantity = $(this).parent().find('#quantity').val();
            quantity++;
            $(this).parent().find('#quantity').val(quantity);
        });
    });


    $('.add_cart').click(function() {
            var product_id = $(this).parent().parent().find('td:first').text();
            var quantity = $(this).parent().parent().find('#quantity').val();

            var data = {
                'quantity': quantity,
                'product_id': product_id
            };
            
            $.ajax({
                url: '<?php echo base_url('dashboard/add_to_cart'); ?>',
                type: 'post',
                data: data,
                success: function(response) {
                    alert(response);
                }
            });
        });
</script>
</html>