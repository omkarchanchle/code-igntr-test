<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin dashboard</title>
    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>
</head>
<body>
    <table>
        <tr>
            <th colspan="2">Active and Verified user count</th>
        </tr>
        <tr>
            <td>count</td>
            <td>
                <?php 
                    $activeUser = $this->db->get_where('register', array('is_active' => 1, 'is_varified' => 1, 'is_admin' => 0))->num_rows();
                    echo $activeUser; 
                ?>
            </td>
        </tr>
    </table>
    <br>
    <table>
        <tr>
            <th colspan="2">User attached with active product</th>
        </tr>
        <tr>
            <td>count</td>
            <td>
                <?php 
                    $userWithProduct = $this->db->query("SELECT count(register.id) as count FROM register JOIN user_product ON register.id=user_product.user_id JOIN product ON user_product.product_id=product.id WHERE register.is_active = 1 AND register.is_varified = 1 AND register.is_admin = 0 AND product.is_active = 1")->row()->count;
                    echo $userWithProduct; 
                ?>
            </td>
        </tr>
    </table>
    <br>
    <table>
        <tr>
            <th colspan="2">Active product count</th>
        </tr>
        <tr>
            <td>count</td>
            <td>
                <?php 
                    $activeProduct = $this->db->get_where('product', array('is_active' => 1))->num_rows();
                    echo $activeProduct; 
                ?>
            </td>
        </tr>
    </table>
    <br>
    <table>
        <tr>
            <th colspan="2">Active product don't belong user</th>
        </tr>
        <tr>
            <td>count</td>
            <td>
                <?php 
                    
                    $productWithNoUser = $this->db->query("SELECT COUNT(id) as count FROM `product` WHERE is_active = 1 AND id NOT IN ( SELECT product_id FROM user_product )")->row()->count;
                    echo $productWithNoUser; 
                ?>
            </td>
        </tr>
    </table>
    <br>
    <table>
        <tr>
            <th colspan="2">Amount of all active attached products</th>
        </tr>
        <tr>
            <td>count</td>
            <td>
                <?php 
                    
                    $producT = $this->db->query("SELECT SUM(user_product.quantity) as total FROM `product` JOIN user_product ON product.id=user_product.product_id JOIN register ON user_product.user_id=register.id WHERE register.is_active = 1 AND register.is_varified = 1 AND register.is_admin = 0 AND product.is_active = 1")->row()->total;
                    echo $producT; 
                ?>
            </td>
        </tr>
    </table>
</body>
</html>