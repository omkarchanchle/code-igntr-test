<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Log in page</title>
</head>
<body>
	<h1>Log in page</h1>
	<form action="<?php echo base_url('login/check'); ?>" method="post">
		<label for="email">Email address</label>
		<input type="email" name="email" id="email">
		<label for="password">Password</label>
		<input type="password" name="password" id="password">
		<input type="submit" value="Log in">
	</form>
</body>
</html>